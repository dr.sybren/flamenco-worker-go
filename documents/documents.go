package documents

import (
	"time"
)

// TODO: decide whether we keep copying the document definitions from
// Flamenco Manager, or import them directly. Currently I decided to
// copy to reduce the number of dependencies.

// The task statuses we recognise.
const (
	StatusQueued           = "queued"
	StatusClaimedByManager = "claimed-by-manager"
	StatusActive           = "active"
	StatusCanceled         = "canceled"
	StatusFailed           = "failed"
	StatusCompleted        = "completed"
	StatusCancelRequested  = "cancel-requested"
)

// Possible worker statuses.
const (
	WorkerStatusStarting = "starting" // signed on but not done anything yet
	WorkerStatusOffline  = "offline"
	WorkerStatusShutdown = "shutdown" // will go offline soon
	WorkerStatusAwake    = "awake"
	WorkerStatusTesting  = "testing"
	WorkerStatusTimeout  = "timeout"
	WorkerStatusAsleep   = "asleep" // listens to a wakeup call, but performs no tasks.
)

// Command is an executable part of a Task
type Command struct {
	Name     string                 `json:"name"`
	Settings map[string]interface{} `json:"settings"`
}

// Task contains a list of commands to execute.
type Task struct {
	ID          string    `json:"_id,omitempty"`
	Etag        string    `json:"_etag,omitempty"`
	Job         string    `json:"job"`
	Manager     string    `json:"manager"`
	Project     string    `json:"project"`
	User        string    `json:"user"`
	Name        string    `json:"name"`
	Status      string    `json:"status"`
	Priority    int       `json:"priority"`
	JobPriority int       `json:"job_priority"`
	JobType     string    `json:"job_type"`
	TaskType    string    `json:"task_type"`
	Commands    []Command `json:"commands"`
	Log         string    `json:"log,omitempty"`
	Activity    string    `json:"activity,omitempty"`
	Parents     []string  `json:"parents,omitempty"`
	Worker      string    `json:"worker,omitempty"`
}

// WorkerRegistration is sent by the Worker to register itself at this Manager.
type WorkerRegistration struct {
	Secret             string   `json:"secret,omitempty"`
	Platform           string   `json:"platform,omitempty"`
	SupportedTaskTypes []string `json:"supported_task_types"`
	Nickname           string   `json:"nickname"`
}

// WorkerInfo contains all information about a specific Worker.
// Some fields come from the WorkerRegistration, whereas others are filled by the Manager.
type WorkerInfo struct {
	ID                 string     `json:"_id,omitempty"`
	Secret             string     `json:"-"`
	HashedSecret       []byte     `json:"-"`
	Nickname           string     `json:"nickname"`
	Address            string     `json:"address"`
	Status             string     `json:"status"`
	Platform           string     `json:"platform"`
	CurrentTask        *string    `json:"current_task,omitempty"`
	TimeCost           int        `json:"time_cost"`
	LastActivity       *time.Time `json:"last_activity,omitempty"`
	SupportedTaskTypes []string   `json:"supported_task_types"`
	Software           string     `json:"software"`

	// For dashboard
	CurrentTaskStatus  string     `json:"current_task_status,omitempty"`
	CurrentTaskUpdated *time.Time `json:"current_task_updated,omitempty"`

	// For controlling sleeping & waking up. For values, see the workerStatusXXX constants.
	StatusRequested string `json:"status_requested"`
}

// WorkerSignonDoc is sent by the Worker upon sign-on.
type WorkerSignonDoc struct {
	SupportedTaskTypes []string `json:"supported_task_types,omitempty"`
	Nickname           string   `json:"nickname,omitempty"`
}

// StateChange is received from Manager when we should change state.
type StateChange struct {
	StatusRequested string `json:"status_requested"`
}

// TaskUpdate is both sent from Worker to Manager.
type TaskUpdate struct {
	TaskID                    string `json:"task_id,omitempty"`
	TaskStatus                string `json:"task_status,omitempty"`
	Activity                  string `json:"activity,omitempty"`
	TaskProgressPercentage    int    `json:"task_progress_percentage"`
	CurrentCommandIdx         int    `json:"current_command_idx"`
	CommandProgressPercentage int    `json:"command_progress_percentage"`
	Log                       string `json:"log,omitempty"`
}

// MayKeepRunningResponse is sent to workers to indicate whether they can keep running their task.
type MayKeepRunningResponse struct {
	MayKeepRunning bool   `json:"may_keep_running"`
	Reason         string `json:"reason,omitempty"`

	// For controlling sleeping & waking up. For values, see the workerStatusXXX constants.
	StatusRequested string `json:"status_requested,omitempty"`
}

// FileProduced is sent to the Manager whenever we produce (e.g. render) some file.
type FileProduced struct {
	Paths []string `json:"paths"`
}
