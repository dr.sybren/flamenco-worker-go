package commands

import (
	"errors"
	"fmt"
)

type Settings map[string]interface{}

// Update is produced by a Runner to indicate a progress report.
type Update struct {
	Activity           string
	ProgressPercentage int
	Log                string
	Err                error
}

type Runner func(
	settings Settings,
	doneChan <-chan struct{},
	updateChan chan<- Update,
) error

var registry map[string]Runner

func init() {
	registry = map[string]Runner{
		"echo":  echo,
		"sleep": sleep,
	}
}

func GetRunner(commandName string) (Runner, bool) {
	runner, found := registry[commandName]
	return runner, found
}

// ErrorUpdate constructs an Update for the given error message.
func ErrorUpdate(msg string, args ...interface{}) Update {
	formatted := fmt.Sprintf(msg, args...)
	return Update{
		Activity: formatted,
		Log:      formatted,
		Err:      errors.New(formatted),
	}
}
