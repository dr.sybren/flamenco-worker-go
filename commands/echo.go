package commands

import (
	"errors"
	"fmt"

	"github.com/sirupsen/logrus"
)

func echoParseSettings(settings Settings) (string, error) {
	message, ok := settings["message"]
	if !ok {
		return "", errors.New("Missing setting 'message'")
	}
	switch v := message.(type) {
	case string:
		return v, nil
	default:
		msg := fmt.Sprintf("bad type for setting 'message', expected str, not %v", v)
		logrus.WithField("message", message).Error(msg)
		return "", errors.New(msg)
	}
}

func echo(
	settings Settings,
	doneChan <-chan struct{},
	updateChan chan<- Update,
) error {
	message, err := echoParseSettings(settings)
	if err != nil {
		return err
	}
	logger := logrus.WithFields(logrus.Fields{
		"command": "echo",
		"message": message,
	})
	logger.Info("logging message")

	msg := fmt.Sprintf("Command 'echo': %s", message)
	updateChan <- Update{Activity: msg, Log: msg}

	return nil
}
