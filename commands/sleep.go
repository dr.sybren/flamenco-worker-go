package commands

import (
	"errors"
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
)

func sleepParseSettings(settings Settings) (time.Duration, error) {
	sleepTime, ok := settings["time_in_seconds"]
	if !ok {
		return 0, errors.New("Missing setting 'time_in_seconds'")
	}
	switch v := sleepTime.(type) {
	case int:
		return time.Duration(v) * time.Second, nil
	default:
		msg := fmt.Sprintf("bad type for setting 'time_in_seconds', expected int, not %v", v)
		logrus.WithField("time_in_seconds", sleepTime).Error(msg)
		return 0, errors.New(msg)
	}
}

func sleep(
	settings Settings,
	doneChan <-chan struct{},
	updateChan chan<- Update,
) error {
	duration, err := sleepParseSettings(settings)
	if err != nil {
		return err
	}
	logger := logrus.WithFields(logrus.Fields{
		"command":  "sleep",
		"duration": duration,
	})
	logger.Info("sleeping")

	msg := fmt.Sprintf("Command 'sleep': sleeping for %v", duration)
	updateChan <- Update{Activity: msg, Log: msg}

	time.Sleep(duration)

	logger.Info("done sleeping")

	return nil
}
