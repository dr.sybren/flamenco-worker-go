module gitlab.com/dr.sybren/flamenco-worker-go

go 1.13

require (
	github.com/kardianos/osext v0.0.0-20170510131534-ae77be60afb1
	github.com/kr/pretty v0.1.0 // indirect
	github.com/sirupsen/logrus v1.2.0
	github.com/stretchr/testify v1.2.2
	gitlab.com/blender-institute/gossdp v0.0.0-20181214124559-074ccf115d76
	golang.org/x/crypto v0.0.0-20181203042331-505ab145d0a9 // indirect
	golang.org/x/net v0.0.0-20181213202711-891ebc4b82d6 // indirect
	golang.org/x/sys v0.0.0-20181213200352-4d1cda033e06 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127
	gopkg.in/yaml.v2 v2.2.2
)
