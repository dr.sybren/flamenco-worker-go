package ssdp

import (
	"net/url"

	"github.com/sirupsen/logrus"
	"gitlab.com/blender-institute/gossdp"
)

// ManagerFinder uses UPnP/SSDP to find a Flamenco Manager on the local network.
type ManagerFinder interface {
	FindFlamencoManager() <-chan *url.URL
}

// Finder is a ManagerFinder that uses  UPnP/SSDP to find a Flamenco Manager on the local network.
type Finder struct {
	overrideURL *url.URL
}

type ssdpClient struct {
	response chan interface{}
}

// NewManagerFinder returns a default SSDP/UPnP based finder.
func NewManagerFinder(managerURL *url.URL) ManagerFinder {
	return Finder{
		overrideURL: managerURL,
	}
}

func (b *ssdpClient) NotifyAlive(message gossdp.AliveMessage) {
	logrus.WithField("message", message).Info("UPnP/SSDP NotifyAlive")
}
func (b *ssdpClient) NotifyBye(message gossdp.ByeMessage) {
	logrus.WithField("message", message).Info("UPnP/SSDP NotifyBye")
}
func (b *ssdpClient) Response(message gossdp.ResponseMessage) {
	logrus.WithField("message", message).Debug("UPnP/SSDP response")
	url, err := url.Parse(message.Location)
	if err != nil {
		b.response <- err
		return
	}
	b.response <- url
}

// FindFlamencoManager tries to find a Manager, sending its URL to the returned channel.
func (f Finder) FindFlamencoManager() <-chan *url.URL {
	reporter := make(chan *url.URL)

	go func() {
		defer close(reporter)

		if f.overrideURL != nil {
			logrus.WithField("url", f.overrideURL.String()).Debug("Using configured Flamenco Manager URL")
			reporter <- f.overrideURL
			return
		}

		logrus.Info("finding Flamenco Manager via UPnP/SSDP")
		b := ssdpClient{make(chan interface{})}

		client, err := gossdp.NewSsdpClientWithLogger(&b, logrus.StandardLogger())
		if err != nil {
			logrus.WithError(err).Fatal("Unable to create UPnP/SSDP client")
			return
		}

		logrus.Debug("Starting UPnP/SSDP client")
		go client.Start()
		defer client.Stop()

		if err := client.ListenFor("urn:flamenco:manager:0"); err != nil {
			logrus.WithError(err).Error("unable to find Manager")
			return
		}

		logrus.Debug("Waiting for UPnP/SSDP answer")
		urlOrErr := <-b.response
		switch v := urlOrErr.(type) {
		case *url.URL:
			reporter <- v
		case error:
			logrus.WithError(v).Fatal("Error waiting for UPnP/SSDP response from Manager")
		}
	}()

	return reporter
}
