package tasks

import (
	"gitlab.com/dr.sybren/flamenco-worker-go/commands"
	"gitlab.com/dr.sybren/flamenco-worker-go/documents"
)

// CommandUpdate is produced by a TaskRunner to indicate a progress report.
type CommandUpdate struct {
	Activity                  string
	CommandProgressPercentage int
	Log                       string
	Err                       error
}

// Runner instances can (test-)run Flamenco tasks.
type Runner interface {
	RunTask(task documents.Task) <-chan documents.TaskUpdate
	ExecCommand(command documents.Command) <-chan commands.Update
	Abort()
}
