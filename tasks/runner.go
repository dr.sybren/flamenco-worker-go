package tasks

import (
	"errors"
	"sync"

	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/flamenco-worker-go/commands"
	"gitlab.com/dr.sybren/flamenco-worker-go/documents"
)

// RealTaskRunner is a real task runner (contrary to testing ones)
type RealTaskRunner struct {
	doneChan chan struct{}
	doneWg   *sync.WaitGroup
}

// ErrNotImplemented is returned when a task runner function hasn't been implemented yet.
var ErrNotImplemented = errors.New("Task runner not implemented")

// NewRunner constructs & returns a task runner.
func NewRunner() Runner {
	return &RealTaskRunner{
		doneChan: make(chan struct{}),
		doneWg:   new(sync.WaitGroup),
	}
}

// Abort stops the task runner and waits until it is done.
func (rtr *RealTaskRunner) Abort() {
	logrus.Info("RealTaskRunner: aborting")
	close(rtr.doneChan)
	rtr.doneWg.Wait()

	// Construct a new 'done' channel so that this runner can be reused.
	rtr.doneChan = make(chan struct{})
}

// RunTask splits a task into commands and executes those, placing updates in a channel.
func (rtr *RealTaskRunner) RunTask(task documents.Task) <-chan documents.TaskUpdate {
	// TODO(Sybren): make this a buffered channel?
	reporter := make(chan documents.TaskUpdate)

	logger := logrus.WithField("task_id", task.ID)

	go func() {
		rtr.doneWg.Add(1)
		defer rtr.doneWg.Done()
		defer close(reporter)

		for commandIdx, command := range task.Commands {
			logFields := logrus.Fields{
				"command_idx":  commandIdx,
				"command_name": command.Name,
			}

			for cmdUpdate := range rtr.ExecCommand(command) {
				taskUpdate := documents.TaskUpdate{
					TaskID:            task.ID,
					CurrentCommandIdx: commandIdx,
					Activity:          cmdUpdate.Activity,
					Log:               cmdUpdate.Log,
				}
				if cmdUpdate.Err != nil {
					logger.WithFields(logFields).WithError(cmdUpdate.Err).Error(
						"error executing command, failing task")
					taskUpdate.TaskStatus = documents.StatusFailed
					reporter <- taskUpdate
					return
				}

				reporter <- taskUpdate
			}
		}

		logger.Info("task completed")
		reporter <- documents.TaskUpdate{
			TaskID:            task.ID,
			CurrentCommandIdx: len(task.Commands),
			Activity:          "Task completed",
			Log:               "Task completed.\n",
			TaskStatus:        documents.StatusCompleted,
		}
	}()

	return reporter
}

// ExecCommand performs a given command in a Goroutine, and places updates in a channel.
func (rtr *RealTaskRunner) ExecCommand(command documents.Command) <-chan commands.Update {
	updateChan := make(chan commands.Update)
	go func() {
		defer close(updateChan)

		rtr.doneWg.Add(1)
		defer rtr.doneWg.Done()

		logger := logrus.WithFields(logrus.Fields{
			"command":  command.Name,
			"settings": command.Settings,
		})

		// Find the command runner function
		runner, found := commands.GetRunner(command.Name)
		if !found {
			logger.Error("no runner for this command implemented")
			updateChan <- commands.ErrorUpdate(
				"no runner for command '%s' implemented", command.Name)
			return
		}

		// Actually run the command
		logger.Info("running command")
		err := runner(command.Settings, rtr.doneChan, updateChan)
		if err != nil {
			logger.Warning("command finished with error")
			updateChan <- commands.ErrorUpdate(err.Error())
			return
		}

		logger.Info("command finished")
	}()
	return updateChan
}
