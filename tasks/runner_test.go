package tasks

import (
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/dr.sybren/flamenco-worker-go/commands"
	"gitlab.com/dr.sybren/flamenco-worker-go/documents"

	check "gopkg.in/check.v1"
)

type RunnerTestSuite struct {
}

var _ = check.Suite(&RunnerTestSuite{})

func (s *RunnerTestSuite) TestSleepEchoTasks(t *check.C) {
	rtr := NewRunner()

	task := documents.Task{
		ID:          "aaaaaabbbbbbccccccdddddd",
		Etag:        "someEtag",
		Job:         "000000000000000000000000",
		Manager:     "111111111111111111111111",
		Project:     "222222222222222222222222",
		User:        "333333333333333333333333",
		Name:        "sleep-and-echo",
		Status:      "active",
		Priority:    100,
		JobPriority: 5,
		JobType:     "unittest-job",
		TaskType:    "unittest-task",
		Commands: []documents.Command{
			documents.Command{
				Name:     "sleep",
				Settings: commands.Settings{"time_in_seconds": 1},
			},
			documents.Command{
				Name:     "echo",
				Settings: commands.Settings{"message": "💩 op je hoofd!"},
			},
		},
	}

	updates := rtr.RunTask(task)

	// We expect three updates, one for each cmd and one to indicate task completion.
	select {
	case update, ok := <-updates:
		assert.True(t, ok)
		assert.Equal(t, 0, update.CurrentCommandIdx)
		assert.Equal(t, "", update.TaskStatus)
		break
	case <-time.After(250 * time.Millisecond):
		assert.FailNow(t, "timeout waiting for result #0")
		break
	}

	// Check the 2nd update.
	select {
	case update, ok := <-updates:
		assert.True(t, ok)
		assert.Equal(t, 1, update.CurrentCommandIdx)
		assert.Equal(t, "", update.TaskStatus)
		assert.Contains(t, update.Activity, "💩 op je hoofd!")
		assert.Contains(t, update.Log, "💩 op je hoofd!")
		break
	case <-time.After(1250 * time.Millisecond):
		assert.FailNow(t, "timeout waiting for result #1")
		break
	}

	// Check the 3rd update.
	select {
	case update, ok := <-updates:
		assert.True(t, ok)
		assert.Equal(t, 2, update.CurrentCommandIdx)
		assert.Equal(t, documents.StatusCompleted, update.TaskStatus)
		break
	case <-time.After(1250 * time.Millisecond):
		assert.FailNow(t, "timeout waiting for result #2")
		break
	}

	// Channel should be closed now.
	select {
	case _, ok := <-updates:
		assert.False(t, ok)
		break
	case <-time.After(250 * time.Millisecond):
		assert.FailNow(t, "timeout waiting for channel close")
		break
	}

}
