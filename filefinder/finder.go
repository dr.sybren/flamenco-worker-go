package finder

import (
	"os"
	"path/filepath"

	"github.com/kardianos/osext"
	"github.com/sirupsen/logrus"
)

// FilePrefix returns the filename prefix to find bundled files.
// Files are searched for relative to the current working directory as well as relative
// to the currently running executable.
func FilePrefix(fileToFind string) string {
	logger := logrus.WithField("file_to_find", fileToFind)

	// Find as relative path, i.e. relative to CWD.
	_, err := os.Stat(fileToFind)
	if err == nil {
		logger.Debug("Found files in current working directory")
		return ""
	}

	// Find relative to executable folder.
	exedirname, err := osext.ExecutableFolder()
	if err != nil {
		logger.WithError(err).Error("unable to determine the executable's directory")
		return ""
	}

	if _, err := os.Stat(filepath.Join(exedirname, fileToFind)); os.IsNotExist(err) {
		cwd, err := os.Getwd()
		if err != nil {
			logger.WithError(err).Error("unable to determine current working directory")
		}
		logger.WithFields(logrus.Fields{
			"cwd":        cwd,
			"exedirname": exedirname,
		}).Error("unable to find file")
		return ""
	}

	// Append a slash so that we can later just concatenate strings.
	logrus.WithField("exedirname", exedirname).Debug("found file")
	return exedirname + string(os.PathSeparator)
}
