package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/flamenco-worker-go/testmode"
	"gitlab.com/dr.sybren/flamenco-worker-go/version"
	"gitlab.com/dr.sybren/flamenco-worker-go/workermode"
)

var shutdownComplete chan struct{}
var worker *workermode.Worker

var cliArgs struct {
	version    bool
	verbose    bool
	debug      bool
	managerURL *url.URL
	manager    string
	register   bool

	quicktest bool   // go to quick test mode
	testfile  string // the file to render a few frames of
	testout   string // the output directory to write the frames to
}

func parseCliArgs() {
	flag.BoolVar(&cliArgs.version, "version", false, "Shows the application version, then exits.")
	flag.BoolVar(&cliArgs.verbose, "verbose", false, "Enable info-level logging.")
	flag.BoolVar(&cliArgs.debug, "debug", false, "Enable debug-level logging.")

	flag.StringVar(&cliArgs.manager, "manager", "", "URL of the Flamenco Manager.")
	flag.BoolVar(&cliArgs.register, "register", false, "(Re-)register at the Manager.")

	flag.BoolVar(&cliArgs.quicktest, "test", false, "Quick test: run a simple test, then exit.")
	flag.StringVar(&cliArgs.testfile, "test-file", "", "Quick test: blend file to render a few frames of.")
	flag.StringVar(&cliArgs.testout, "test-out", ".", "Quick test: directory to write frames to.")

	flag.Parse()

	if cliArgs.manager != "" {
		var err error
		logger := logrus.WithField("url", cliArgs.manager)
		cliArgs.managerURL, err = workermode.ParseURL(cliArgs.manager)
		if err != nil {
			logger.WithError(err).Fatal("invalid URL")
		}
	}
}

func configLogging() {
	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
	})

	// Only log the warning severity or above by default.
	var level logrus.Level
	switch {
	case cliArgs.debug:
		level = logrus.DebugLevel
	case cliArgs.verbose:
		level = logrus.InfoLevel
	default:
		level = logrus.WarnLevel
	}
	logrus.SetLevel(level)
	log.SetOutput(logrus.StandardLogger().Writer())
}

func logStartup() {
	level := logrus.GetLevel()
	logrus.SetLevel(logrus.InfoLevel)
	defer logrus.SetLevel(level)

	logrus.WithFields(logrus.Fields{
		"managerURL": cliArgs.managerURL,
		"version":    version.ApplicationVersion,
		"pid":        os.Getpid(),
		"GOOS":       runtime.GOOS,
		"GOARCH":     runtime.GOARCH,
	}).Info("starting Flamenco Worker")
}

func shutdown(signum os.Signal) {
	done := make(chan struct{})
	go func() {
		logrus.WithField("signal", signum).Info("signal received, shutting down.")

		if worker != nil {
			worker.AckShutdown()
			worker.Close()
		}
		close(done)
	}()

	select {
	case <-done:
		logrus.Debug("shutdown OK")
	case <-time.After(20 * time.Second):
		logrus.Error("shutdown forced, stopping process.")
		os.Exit(-2)
	}

	logrus.Warning("shutdown complete, stopping process.")
	close(shutdownComplete)
}

func main() {
	parseCliArgs()
	if cliArgs.version {
		fmt.Println(version.ApplicationVersion)
		return
	}

	configLogging()
	logStartup()

	// Set some more or less sensible limits & timeouts.
	http.DefaultTransport = &http.Transport{
		MaxIdleConns:          100,
		TLSHandshakeTimeout:   3 * time.Second,
		IdleConnTimeout:       15 * time.Minute,
		ResponseHeaderTimeout: 15 * time.Second,
	}

	if cliArgs.quicktest {
		success := testmode.TestMode(testmode.Config{
			BlendFile: cliArgs.testfile,
			Outpath:   cliArgs.testout,
		})

		if !success {
			os.Exit(47)
		}
		return
	}

	worker = workermode.Run(cliArgs.managerURL, cliArgs.register)

	shutdownComplete = make(chan struct{})

	// Handle Ctrl+C
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)
	go func() {
		for signum := range c {
			// Run the shutdown sequence in a goroutine, so that multiple Ctrl+C presses can be handled in parallel.
			go shutdown(signum)
		}
	}()

	<-shutdownComplete
	logrus.Debug("stopping main")
}
