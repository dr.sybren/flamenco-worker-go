package version

var (
	// ApplicationVersion is the version of Flamenco Worker.
	ApplicationVersion = "set-during-build"

	// UserAgent should be used in all HTTP requests.
	UserAgent = "Flamenco-Worker-Go/" + ApplicationVersion
)
