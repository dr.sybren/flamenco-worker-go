package testmode

import (
	"github.com/sirupsen/logrus"
)

// Config contains the test mode configuration paramaters.
type Config struct {
	BlendFile string
	Outpath   string
}

// TestMode performs a render test, then returns true if ok and false otherwise.
func TestMode(c Config) bool {
	logrus.WithField("config", c).Info("Entering test mode")

	// TODO: Implement

	logrus.Info("Done with test, exiting")
	return true
}
