package workermode

import (
	"github.com/sirupsen/logrus"
)

const (
	stateAsleep   = "asleep"
	stateAwake    = "awake"
	stateShutdown = "shutdown"
	stateStarting = "starting"
)

func (w *Worker) setupStateMachine() {
	w.stateStarters[stateAsleep] = w.gotoStateAsleep
	w.stateStarters[stateAwake] = w.gotoStateAwake
	w.stateStarters[stateShutdown] = w.gotoStateShutdown
}

// Called whenever the Flamenco Manager has a change in current status for us.
func (w *Worker) changeState(newState string) {
	w.stateMutex.Lock()
	logger := logrus.WithFields(logrus.Fields{
		"new_state": newState,
		"cur_state": w.state,
	})
	w.stateMutex.Unlock()

	logger.Info("state change requested by Manager")

	starter, ok := w.stateStarters[newState]
	if !ok {
		logger.WithField("available", w.stateStarters).Warning("no state starter for this state, going to sleep instead")
		starter = w.gotoStateAsleep
	}
	starter()
}

// Confirm that we're now in a certain state.
//
// This ACK can be given without a request from the server, for example to support
// state changes originating from UNIX signals.
//
// The state is passed as string so that this function can run independently of
// the current w.state (for thread-safety)
func (w *Worker) ackStateChange(state string) {
	w.doneWg.Add(1)
	defer w.doneWg.Done()

	logger := logrus.WithField("state", state)
	logger.Debug("notifying Manager of our state")
	err := w.SendJSON(logger, "POST", "/ack-status-change/"+state, nil, nil, nil)
	if err != nil {
		logger.WithError(err).Warning("unable to notify Manager of status change")
	}
}

func (w *Worker) isState(state string) bool {
	w.stateMutex.Lock()
	defer w.stateMutex.Unlock()
	return w.state == state
}
