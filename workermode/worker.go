package workermode

import (
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"os"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/flamenco-worker-go/documents"
	"gitlab.com/dr.sybren/flamenco-worker-go/ssdp"
	"gitlab.com/dr.sybren/flamenco-worker-go/tasks"
)

const (
	requestRetry        = 5 * time.Second
	credentialsFilename = "flamenco-worker-credentials.yaml"
	configFilename      = "flamenco-worker.yaml"
)

var (
	errRequestAborted = errors.New("request to Manager aborted")
)

// Worker performs regular Flamenco Worker operations.
type Worker struct {
	doneChan chan struct{}
	doneWg   *sync.WaitGroup

	manager *url.URL
	client  *http.Client
	creds   *workerCredentials

	state         string
	stateStarters map[string]func() // gotoStateXXX functions
	stateMutex    *sync.Mutex

	taskRunner tasks.Runner

	configWrangler ConfigWrangler
	config         WorkerConfig
	managerFinder  ssdp.ManagerFinder
}

// NewWorker constructs and returns a new Worker.
func NewWorker(configWrangler ConfigWrangler, managerFinder ssdp.ManagerFinder,
	taskRunner tasks.Runner) *Worker {
	worker := &Worker{
		doneChan: make(chan struct{}),
		doneWg:   new(sync.WaitGroup),

		client: &http.Client{},

		state:         stateStarting,
		stateStarters: make(map[string]func()),
		stateMutex:    new(sync.Mutex),

		taskRunner: taskRunner,

		configWrangler: configWrangler,
		managerFinder:  managerFinder,
	}
	worker.setupStateMachine()
	worker.loadConfig()
	return worker
}

func (w *Worker) start(register bool) {
	go func() {
		w.doneWg.Add(1)
		defer w.doneWg.Done()

		w.loadCredentials()

		if w.creds == nil || register {
			w.register()
			w.changeState(stateAwake)
		} else {
			startState := w.signOn()
			w.changeState(startState)
		}

	}()
}

func (w *Worker) loadCredentials() {
	logrus.Debug("loading credentials")

	w.creds = &workerCredentials{}
	err := w.configWrangler.LoadConfig(credentialsFilename, w.creds)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			logrus.ErrorKey: err,
			"file":          credentialsFilename,
		}).Warning("unable to load credentials configuration file")
		w.creds = nil
		return
	}
}

func (w *Worker) loadConfig() {
	logger := logrus.WithField("filename", configFilename)
	err := w.configWrangler.LoadConfig(configFilename, &w.config)
	if os.IsNotExist(err) {
		logger.Info("writing default configuration file")
		w.config = w.configWrangler.DefaultConfig()
		w.saveConfig()
		err = w.configWrangler.LoadConfig(configFilename, &w.config)
	}
	if err != nil {
		logger.WithError(err).Fatal("unable to load config file")
	}

	if w.config.Manager != "" {
		w.manager, err = ParseURL(w.config.Manager)
		if err != nil {
			logger.WithFields(logrus.Fields{
				logrus.ErrorKey: err,
				"url":           w.config.Manager,
			}).Fatal("unable to parse manager URL")
		}
		logger.WithField("url", w.config.Manager).Debug("parsed manager URL")
	}

}

func (w *Worker) saveConfig() {
	err := w.configWrangler.WriteConfig(configFilename, "Configuration", w.config)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			logrus.ErrorKey: err,
			"filename":      configFilename,
		}).Error("unable to write configuration file")
	}
}

func (w *Worker) managerRequest(
	urlSuffix string,
	payload interface{},
	responsehandler responsehandler,
	repeatUntilOk bool,
	logger logrus.FieldLogger,
) (finalError error) {
	logger = logger.WithField("url", urlSuffix)
	requestDone := make(chan struct{})

	go func() {
		w.doneWg.Add(1)
		defer w.doneWg.Done()
		for {
			select {
			case <-w.doneChan:
				logger.Debug("request aborted")
				return
			default:
				// ignore
			}

			sendErr := w.SendJSON(logger, "POST", urlSuffix, payload, nil, responsehandler)
			if sendErr == nil {
				close(requestDone)
				break
			}

			if !repeatUntilOk {
				finalError = sendErr
				close(requestDone)
				break
			}
			logger.WithField("retry_after", requestRetry).Info("error communicating with Manager, will retry later")
			time.Sleep(requestRetry)
		}
	}()

	select {
	case <-requestDone:
		logger.Debug("request done")
	case <-w.doneChan:
		logger.Debug("request aborted")
		return errRequestAborted
	}

	return
}

// (Re-)register ourselves at the Manager.
func (w *Worker) register() {
	// Construct our new password.
	secret := make([]byte, 32)
	if _, err := rand.Read(secret); err != nil {
		logrus.WithError(err).Fatal("unable to generate secret key")
	}
	secretKey := hex.EncodeToString(secret)

	// TODO: load taskTypes from config file.
	taskTypes := []string{"unknown", "sleep", "blender-render", "debug", "ffmpeg"}
	reginfo := documents.WorkerRegistration{
		Nickname:           hostname(),
		Platform:           platform(),
		Secret:             secretKey,
		SupportedTaskTypes: taskTypes,
	}

	logrus.WithFields(logrus.Fields{
		"nickname":   reginfo.Nickname,
		"platform":   reginfo.Platform,
		"task_types": reginfo.SupportedTaskTypes,
	}).Info("registering new account")

	var registrationResponse documents.WorkerInfo
	handleResponse := func(resp *http.Response, body []byte) error {
		logrus.WithField("status_code", resp.StatusCode).Debug("registration response received")
		return json.Unmarshal(body, &registrationResponse)
	}

	logger := logrus.WithField("manager", w.manager.String())
	err := w.managerRequest("/register-worker", &reginfo, handleResponse, true, logger)
	if err != nil {
		logger.WithError(err).Fatal("unable to register")
	}

	// store ID and secretKey in config file when registration is complete.
	err = w.configWrangler.WriteConfig(credentialsFilename, "Credentials", workerCredentials{
		WorkerID: registrationResponse.ID,
		Secret:   secretKey,
	})
	if err != nil {
		logrus.WithFields(logrus.Fields{
			logrus.ErrorKey: err,
			"file":          credentialsFilename,
		}).Fatal("unable to write credentials configuration file")
	}
}

func (w *Worker) reregister() {
	w.register()
	w.loadConfig()
}

func (w *Worker) signOn() string {
	logger := logrus.WithField("manager", w.manager.String())
	logger.Info("signing on at Manager")

	if w.creds == nil {
		logger.Fatal("no credentials, unable to sign on")
	}

	// TODO: load taskTypes from config file.
	taskTypes := []string{"unknown", "sleep", "blender-render", "debug", "ffmpeg"}
	payload := documents.WorkerRegistration{
		Nickname:           hostname(),
		SupportedTaskTypes: taskTypes,
	}

	startupState := stateAwake
	err := w.managerRequest("/sign-on", &payload, func(resp *http.Response, body []byte) error {
		if resp.StatusCode != 200 {
			return nil
		}

		stateChange := documents.StateChange{}
		if err := json.Unmarshal(body, &stateChange); err != nil {
			return err
		}
		startupState = stateChange.StatusRequested
		return nil
	}, true, logger)
	if err != nil {
		logger.WithError(err).Fatal("unable to sign on")
	}

	logger.WithField("startup_state", startupState).Info("manager accepted sign-on")
	return startupState
}

// Close gracefully shuts down the Worker.
func (w *Worker) Close() {
	logrus.Debug("worker gracefully shutting down")
	close(w.doneChan)
	w.doneWg.Wait()
	logrus.Debug("worker shut down")
}

func (w *Worker) findManager() {
	if w.manager != nil {
		logrus.WithField("manager_url", w.manager).Debug("Manager URL known")
		return
	}

	finder := w.managerFinder.FindFlamencoManager()
	select {
	case w.manager = <-finder:
		break
	case <-time.After(10 * time.Second):
		logrus.Error("unable to autodetect Flamenco Manager via UPnP/SSDP; configure the URL explicitly")
		panic("no manager found")
	}

	return
}
