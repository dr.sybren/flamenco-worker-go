package workermode

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/flamenco-worker-go/documents"
)

const durationSleepCheck = 3 * time.Second

func (w *Worker) gotoStateAsleep() {
	w.stateMutex.Lock()
	defer w.stateMutex.Unlock()

	w.state = stateAsleep
	// TODO: stop fetching tasks
	go w.ackStateChange(w.state)
	go w.runStateAsleep()
}

func (w *Worker) runStateAsleep() {
	w.doneWg.Add(1)
	defer w.doneWg.Done()

	logger := logrus.WithField("module", stateAsleep)
	keepSleeping := true

	handleResponse := func(resp *http.Response, body []byte) error {
		sublogger := logger.WithField("status_code", resp.StatusCode)

		switch resp.StatusCode {
		case 204:
			sublogger.Debug("continue sleeping")
		case 200:
			newStateInfo := &documents.StateChange{}
			if err := json.Unmarshal(body, &newStateInfo); err != nil {
				return err
			}
			sublogger.Debug("waking up")
			keepSleeping = false
			w.changeState(newStateInfo.StatusRequested)
		default:
			sublogger.WithField("body", string(body)).Warning("unknown response received")
		}

		return nil
	}

	logger.Info("sleeping")
	for keepSleeping {
		select {
		case <-time.After(durationSleepCheck):
			if !w.isState(stateAsleep) {
				logger.Debug("sleep interrupted by state change")
				return
			}
		case <-w.doneChan:
			logger.Debug("sleep interrupted by shutdown")
			return
		}

		logger.Debug("checking with Manager whether we can continue sleeping")
		err := w.SendJSON(logger, "GET", "/status-change", nil, nil, handleResponse)
		if err != nil {
			logger.WithError(err).Warning("error checking status on Manager")
		}
	}
}
