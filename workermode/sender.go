package workermode

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/flamenco-worker-go/version"
)

// SendJSON sends a JSON document to some URL via HTTP.
// :param tweakrequest: can be used to tweak the request before sending it, for
//    example by adding authentication headers. May be nil.
// :param responsehandler: is called when a non-error response has been read.
//    May be nil.
func (w *Worker) SendJSON(
	logger logrus.FieldLogger,
	method string,
	urlSuffix string,
	payload interface{},
	tweakrequest func(req *http.Request),
	responsehandler responsehandler,
) error {
	var payloadBuffer *bytes.Buffer
	if payload == nil {
		payloadBuffer = bytes.NewBuffer([]byte{})
	} else {
		payloadBytes, err := json.Marshal(payload)
		if err != nil {
			logger.WithError(err).Error("unable to marshal JSON")
			return err
		}
		payloadBuffer = bytes.NewBuffer(payloadBytes)
	}

	url, err := w.manager.Parse(urlSuffix)
	if err != nil {
		return err
	}

	logger = logger.WithFields(logrus.Fields{
		"url":    url.String(),
		"method": method,
	})
	// TODO Sybren: enable GZip compression.
	req, err := http.NewRequest(method, url.String(), payloadBuffer)
	if err != nil {
		logger.WithError(err).Error("unable to create request")
		return err
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("User-Agent", version.UserAgent)
	if tweakrequest != nil {
		tweakrequest(req)
	}

	if w.creds != nil {
		username := w.creds.WorkerID
		req.SetBasicAuth(username, w.creds.Secret)
	}

	resp, err := w.client.Do(req)
	if err != nil {
		logger.WithError(err).Warning("unable to perform request")
		return err
	}
	logger = logger.WithField("http_status", resp.StatusCode)

	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		logger.WithError(err).Warning("error reading response")
		return err
	}

	if responsehandler != nil {
		return responsehandler(resp, body)
	}

	if resp.StatusCode >= 400 {
		if resp.StatusCode != 404 {
			logger = logger.WithField("body", string(body))
		}
		logger.WithField("status", resp.StatusCode).Warning("error response received")
		return fmt.Errorf("error %d %sing %s", resp.StatusCode, method, url.String())
	}

	return nil
}
