package workermode

import (
	"github.com/stretchr/testify/assert"

	check "gopkg.in/check.v1"
)

type ConfigTestSuite struct {
}

var _ = check.Suite(&ConfigTestSuite{})

func (s *ConfigTestSuite) TestParseURL(t *check.C) {
	test := func(expected, input string) {
		actualURL, err := ParseURL(input)
		assert.Nil(t, err)
		assert.Equal(t, expected, actualURL.String())
	}

	test("http://jemoeder:1234", "jemoeder:1234")
	test("http://jemoeder/", "jemoeder")
	test("opjehoofd://jemoeder:4213/xxx", "opjehoofd://jemoeder:4213/xxx")
}
