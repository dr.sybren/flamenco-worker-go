package workermode

import (
	"os"

	"github.com/sirupsen/logrus"
)

func (w *Worker) gotoStateShutdown() {
	w.stateMutex.Lock()
	defer w.stateMutex.Unlock()

	w.state = stateShutdown

	logger := logrus.WithField("pid", os.Getpid())
	proc, err := os.FindProcess(os.Getpid())
	if err != nil {
		logger.WithError(err).Fatal("unable to find our own process for clean shutdown")
	}

	logger.Warning("sending our own process an interrupt signal")
	err = proc.Signal(os.Interrupt)
	if err != nil {
		logger.WithError(err).Fatal("unable to find send interrupt signal to our own process")
	}
}

// AckShutdown forces the worker in shutdown state and acknlowedges this to the Manager.
// Does NOT actually peform a shutdown; is intended to be called while shutdown is in progress.
func (w *Worker) AckShutdown() {
	w.stateMutex.Lock()
	w.state = stateShutdown
	logger := logrus.WithField("state", w.state)
	w.stateMutex.Unlock()

	logger.Info("signing off at Manager")
	w.managerRequest("/sign-off", nil, nil, false, logger)
}
