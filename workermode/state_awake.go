package workermode

import (
	"encoding/json"
	"errors"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/flamenco-worker-go/documents"
)

const (
	durationFetchTaskCheck = 5 * time.Second
	durationNoTask         = 5 * time.Second // how long to wait to fetch another task if there is no task now.
	durationFetchFailed    = 5 * time.Second
)

var (
	errUnknownTaskRequestStatus = errors.New("unknown task request status")
	errReregistrationRequired   = errors.New("re-registration is required")
)

func (w *Worker) gotoStateAwake() {
	w.stateMutex.Lock()
	defer w.stateMutex.Unlock()

	w.state = stateAwake

	go w.ackStateChange(w.state)
	go w.runStateAwake()
}

func (w *Worker) runStateAwake() {
	w.doneWg.Add(1)
	defer w.doneWg.Done()

	taskchan := w.fetchTasks()
	for task := range taskchan {
		logrus.WithField("task", task.ID).Info("received task")
	}
}

func (w *Worker) fetchTasks() <-chan documents.Task {
	logger := logrus.WithField("module", stateAwake)
	logger.WithField("manager", w.manager.String()).Info("fetching tasks")

	taskchan := make(chan documents.Task)

	go func() {
		w.doneWg.Add(1)
		defer w.doneWg.Done()
		defer close(taskchan)

		var wait time.Duration

		for {
			select {
			case <-w.doneChan:
				logger.Debug("task fetching interrupted by shutdown")
				return
			case <-time.After(wait):
			}
			if !w.isState(stateAwake) {
				logger.Debug("task fetching interrupted by state change")
				return
			}

			var task *documents.Task
			err := w.SendJSON(logger, "POST", "/task", nil, nil, func(resp *http.Response, body []byte) error {
				switch resp.StatusCode {
				case http.StatusNoContent:
					return nil
				case http.StatusOK:
					*task = documents.Task{}
					return json.Unmarshal(body, task)
				case http.StatusUnauthorized:
					logger.WithField("status_code", resp.StatusCode).Warning("re-registration required")
					w.reregister()
					return nil
				case http.StatusLocked:
					stateChange := documents.StateChange{}
					if err := json.Unmarshal(body, &stateChange); err != nil {
						return err
					}
					w.changeState(stateChange.StatusRequested)
					return nil
				default:
					logger.WithFields(logrus.Fields{
						"status_code": resp.StatusCode,
						"body":        string(body),
					}).Warning("unknown response from Manager")
					return errUnknownTaskRequestStatus
				}
			})
			if err != nil {
				logger.WithError(err).Warning("unable to fetch task to execute")
				// TODO: implement some exponential back-off.
				wait = durationFetchFailed
				continue
			}

			if task != nil {
				taskchan <- *task
			} else {
				logger.WithField("wait", durationNoTask).Info("no tasks to perform, going to wait")

				wait = durationNoTask
			}
		}
	}()

	return taskchan
}
