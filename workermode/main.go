package workermode

import (
	"net/http"
	"net/url"
	"os"

	"github.com/sirupsen/logrus"
)

type responsehandler func(resp *http.Response, body []byte) error

// Run starts the normal operation of the Worker.
func Run(managerURL *url.URL, register bool) *Worker {
	worker := InitializeWorkerMode(managerURL)
	worker.findManager()
	worker.start(register)
	return worker
}

func hostname() string {
	hostname, err := os.Hostname()
	if err != nil {
		logrus.WithError(err).Error("unable to determine hostname")
		return "unknown"
	}
	return hostname
}

func platform() string {
	return platformName
}
