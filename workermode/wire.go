package workermode

import (
	"net/url"

	"gitlab.com/dr.sybren/flamenco-worker-go/ssdp"
	"gitlab.com/dr.sybren/flamenco-worker-go/tasks"
)

// InitializeWorkerMode constructs a Worker instance for normal operation.
func InitializeWorkerMode(managerURL *url.URL) *Worker {
	configWrangler := NewConfigWrangler()
	managerFinder := ssdp.NewManagerFinder(managerURL)
	taskRunner := tasks.NewRunner()
	worker := NewWorker(configWrangler, managerFinder, taskRunner)
	return worker
}
